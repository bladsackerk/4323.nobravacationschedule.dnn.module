﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Coesolutions.Modules.NobraVacationSchedule.View" %>

<%--<link href="/DesktopModules/NobraVacationSchedule/module.css" rel="stylesheet" />
<script src=  "/DesktopModules/NobraVacationSchedule/js/kendo.core.min.js"></script>
<link href="/DesktopModules/NobraVacationSchedule/styles/kendo.common.min.css" rel="stylesheet" />
<link href="/DesktopModules/NobraVacationSchedule/styles/kendo.default.min.css" rel="stylesheet" />
<script src="/DesktopModules/NobraVacationSchedule/js/kendo.all.min.js"></script>
<script src="/DesktopModules/NobraVacationSchedule/content/shared/js/jszip.js"></script>
<script src="/DesktopModules/NobraVacationSchedule/content/shared/js/pako.min.js"></script>
<script src="/DesktopModules/NobraVacationSchedule/js/moment.js"></script>
--%>

<link href="/nobra/DesktopModules/NobraVacationSchedule/module.css" rel="stylesheet" />
<script src="/nobra/DesktopModules/NobraVacationSchedule/js/kendo.core.min.js"></script>
<link href="/nobra/DesktopModules/NobraVacationSchedule/styles/kendo.common.min.css"" rel="stylesheet" />
<link href="/nobra/DesktopModules/NobraVacationSchedule/styles/kendo.default.min.css" rel="stylesheet" />
<script src="/nobra/DesktopModules/NobraVacationSchedule/js/kendo.all.min.js"></script>
<script src="/nobra/DesktopModules/NobraVacationSchedule/content/shared/js/jszip.js"></script>
<script src="/nobra/DesktopModules/NobraVacationSchedule/content/shared/js/pako.min.js"></script>
<script src="/nobra/DesktopModules/NobraVacationSchedule/js/moment.js"></script>



<script>

    $(document).ready(function () {
        //console.log("ready!");
        loadComboBoxesVac();

        $("#btnFetchVacation").kendoButton({
            click: function (e) {
                //alert(e.event.target.tagName);

                if ($("#groupselectionVac").val().length > 0 && $("#yearselectionVac").val().length > 0) {
                    getDataVac($("#groupselectionVac").val(),
                        $("#yearselectionVac").val());
                }
                else {
                    alert('check your selections');
                }





            }
        });


    });



    function loadComboBoxesVac() {
        $("#groupselectionVac").kendoComboBox({
            dataSource: {
                data: [
                    "A"
                    , "B"
                ]
            }
        });



        $("#yearselectionVac").kendoComboBox({
            dataSource: {
                data: [
                    "2016"
                    , "2017"
                     , "2018"
                      , "2019"
                       , "2020"
                        , "2021"

                ]
            }
        });
    }




    function getDataVac(g, y) {


        var paramsVac = {
            group: g,
            year: y
        };


        var me = location.host;
        var myUrl = "/DesktopModules/NOBRADataService/API/Main/GetVacationsByGroupYear"

        if (me.indexOf('nobra') >= 0) {
            myUrl = '/nobra' + myUrl;
        }


        $.ajax({
            url: myUrl,
            type: 'GET',
            data: paramsVac,
            dataType: 'json',
            success: function (data) {
                //WriteResponse(data);
                // alert('Happy, Happy');
                parseDataVac(data);



            },
            error: function () {
                alert('Error on data call');
            }
        });

    }


    function parseDataVac(obj) {

        //for (var Group in obj) {
        //    if (obj.hasOwnProperty(Group)) {
        //        alert(Group + " -> " + obj[Group]);
        //    }
        //}

        var col = obj.VacationLists;


        //var col0 = col[0].WatchGroup;
        //alert('Watchgroup is ' + col0);
        for (var i = 0; i < col.length; i++) {
            // console.log('Watchgroup is ' + col[i].WatchGroup);
            bindGrid(i, col[i]);
        }



    }

    function bindGrid(gid, vacl) {

        var strTitle = $("#groupselectionVac").val() + "-Watch " + $("#yearselectionVac").val() + " Vacations";
        $('#divTitleVacation').html(strTitle);

        //$("#ha" + gid).html(vacl.WatchGroup);
        // $("#hb" + gid).html(vacl.StartDate);

        var fldTitle = vacl.WatchGroup;
        var fldValue = vacl.StartDate;              //vacl.StartDate;          //String(moment(vacl.StartDate).format("MM/DD/YYYY")) //vacl.StartDate;
        // $("#hb" + gid).html

        //    var headerJSON = {
        //        fldTitle: fldValue
        //};



        var headerRecordsVac = new kendo.data.DataSource({



            //schema: {
            //    model: {
            //        fields: {

            //            fldTitle: { type: "string" }

            //        }
            //    }
            //},

            data: [
                {
                    fldTitle: fldTitle
                },
                 {
                     fldTitle: String(fldValue)
                 }


            ]

            //headerJSON
        });

        $("#haVac" + gid).kendoGrid({
            //columns: [
            //               {
            //        field: fldTitle,
            //        //format: "{0: MM/DD/YYYY}"

            //  }
            //],
            dataSource: headerRecordsVac,

            scrollable: false

        }).addClass("hdr");;



        var vacationrecords = new kendo.data.DataSource({



            schema: {
                model: {
                    fields: {
                        PilotName: { type: "string" }

                    }
                }
            },

            data: vacl.Vacations
        });

        $("#gridVacation" + gid).kendoGrid({
            columns: [
              {
                  field: "PilotName"
                  , width: "200px"
              }
            ],
            dataSource: vacationrecords,

            scrollable: false

        });

        $('#retrieveTimeVac').html(moment().format('MM/DD/YYYY HH:mm:ss'));
    }

    function getFileNameString() {

        var vacationFileName = "Vacation_Group_" + String($("#groupselectionVac").val()) + "_" + String($("#yearselectionVac").val()) + ".pdf";

        return vacationFileName
    }

    </script>


<script>
   
</script>

<input id="groupselectionVac" placeholder="Select Group..." style="width: 20%;" />
<input id="yearselectionVac" placeholder="Select Year..." style="width: 20%;" />
<button id="btnFetchVacation" type="button">Retrieve</button>
<button id="btnVacationPDF" type="button">Export to PDF</button>


<div id="printVacation">
    <br />
<table class="vtable">
    <tr>
        <th colspan="4">    <div id="divTitleVacation"  style="text-align:center;border:none";font-size: 36px;>Select and Retrieve For Display</div>
</th>
    </tr>
     <tr>
        <th colspan="4">    <div id="divBaseVacation"  style="text-align:center;border:none";font-size: 36px;>Base Schedule</div>
</th>
    </tr>
    <tr id="0">
        <td>
             <div id="haVac0"></div>
            <div id="gridVacation0"></div>
        </td>
         <td>
             <div id="haVac13"></div>
            <div id="gridVacation13"></div>
        </td>
        <td>
             <div id="haVac1"></div>
            <div id="gridVacation1"></div>
        </td>
         <td>
             <div id="haVac14"></div>
            <div id="gridVacation14"></div>
        </td>

        
         
      

    </tr>
<tr id="1">
       <td>
             <div id="haVac2"></div>
            <div id="gridVacation2"></div>
        </td>
    <td>
             <div id="haVac15"></div>
            <div id="gridVacation15"></div>
        </td>
    <td>
             <div id="haVac3"></div>
            <div id="gridVacation3"></div>
        </td>
     <td>
             <div id="haVac16"></div>
            <div id="gridVacation16"></div>
        </td>


        
   
    
       

    </tr>
<tr id="2">
      
        
       <td>
             <div id="haVac4"></div>
            <div id="gridVacation4"></div>
        </td>
        <td>
             <div id="haVac17"></div>
            <div id="gridVacation17"></div>
        </td>
      <td>
             <div id="haVac5"></div>
            <div id="gridVacation5"></div>
        </td>
      <td>
             <div id="haVac18"></div>
            <div id="gridVacation18"></div>
        </td>
    
    

    </tr>
<tr id ="3">
     <td>
             <div id="haVac6"></div>
            <div id="gridVacation6"></div>
        </td>
     <td>
             <div id="haVac19"></div>
            <div id="gridVacation19"></div>
        </td>
      <td>
             <div id="haVac7"></div>
            <div id="gridVacation7"></div>
        </td>
   
        
       <td>
             <div id="haVac20"></div>
            <div id="gridVacation20"></div>
        </td>
         
     

    </tr>
<tr id="4">
    
          <td>
             <div id="haVac8"></div>
            <div id="gridVacation8"></div>
        </td>
       <td>
             <div id="haVac21"></div>
            <div id="gridVacation21"></div>
        </td>
        
      <td>
             <div id="haVac9"></div>
            <div id="gridVacation9"></div>
        </td>
       
    <td>
             <div id="haVac22"></div>
            <div id="gridVacation22"></div>
        </td>
    </tr>
<tr id="5">
       <td>
             <div id="haVac10"></div>
            <div id="gridVacation10"></div>
        </td>
    <td>
             <div id="haVac23"></div>
            <div id="gridVacation23"></div>
        </td>
       
      <td>
             <div id="haVac11"></div>
            <div id="gridVacation11"></div>
        </td> 
         <td>
             <div id="haVac24"></div>
            <div id="gridVacation24"></div>
        </td>
        

    </tr>
<tr id="6">
    <td>
             <div id="haVac12"></div>
            <div id="gridVacation12"></div>
        </td>
    
        
        <td>
             <div id="haVac25"></div>
            <div id="gridVacation25"></div>
        </td>
        <td>
            <div ></div>
        </td>
        <td>
            <div ></div>
        </td>

    </tr>

</table>
    <div id="retrieveTimeVac"></div>

</div>

<br />


<script>
    var me = location.host;
    var myPostURL = "/DesktopModules/PugetSoundDataService/API/Main/Post";

    if (me.indexOf('nobra') >= 0) {
        myPostURL = '/nobra' + myPostURL;
    }

    //$("#groupselection").val().length > 0 && $("#yearselection").val()





    $("#btnVacationPDF").kendoButton();
    var button = $("#btnVacationPDF").data("kendoButton");
    button.bind("click", function (e) {
        kendo.drawing.drawDOM($('#printVacation'))
            .then(function (group) {
                var PAGE_RECT = new kendo.geometry.Rect(
           [0, 0], [i2p(8 - .5), i2p(10.5 - .5)]
         );
                var content = new kendo.drawing.Group();
                content.append(group);

                kendo.drawing.fit(content, PAGE_RECT)
                //kendo.drawing.pdf.saveAs(group, 'JobReport.pdf');
                // Render the result as a PDF file
                return kendo.drawing.exportPDF(content, {
                    paperSize: "letter",
                    margin: { left: "1cm", top: "1cm", right: "1cm", bottom: "1cm" }
                });
            })
            .done(function (data) {
                // Save the PDF file
                kendo.saveAs({
                    dataURI: data,
                    fileName: getFileNameString(),
                    proxyURL: myPostURL,
                });
            });
    });
</script>

  <script>
      function i2p(val) {
          return val * 72;
      }


    </script>